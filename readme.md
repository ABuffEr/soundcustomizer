# Sound Customizer #

An addon that lets you to customize as you like the NVDA .wav sounds, and more exactly: browseMode.wav, error.wav, exit.wav, focusMode.wav and start.wav.

The addon takes care of everything, you will just have to choose the sounds as you prefer from addon settings.

If you currently have customized sounds, please restore the default ones before installing this addon.

WARNING:

If you customize the start sound, note that:

* When you launch your portable NVDA, that  has just been updated, you will hear the default start sound; the addon knows it, and alerts you that all has been fixed, if you access its settings.

* When you restart NVDA after Sound Customizer uninstallation (via addon manager in tools), the default start sound will not come back yet.

In both cases, restart NVDA and all will work correctly.

For developers, it's due to fact that NVDA plays the start sound before the addons initialization.

## Changes for 1.0 ##
* Initial version.