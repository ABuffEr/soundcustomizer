# Sound Customizer #

Un addon che ti permette di personalizzare a piacere i suoni in .wav di NVDA, e pi&ugrave; precisamente: browseMode.wav, error.wav, exit.wav, focusMode.wav e start.wav.

L'addon si occupa di tutto, dovrai solo scegliere i suoni preferiti dalle sue impostazioni.

Se hai gi&agrave; dei suoni personalizzati, per favore ripristina quelli di default prima di installare questo addon.

ATTENZIONE:

Se personalizzi il suono di avvio, nota che:

* Quando lanci NVDA portable, appena aggiornato, sentirai il suono di avvio di default; l'addon lo sa, e ti avverte che tutto &egrave; stato sistemato, se accedi alle impostazioni.

* Quando riavvii NVDA dopo la disinstallazione di Sound Customizer (attraverso il gestore componenti aggiuntivi in strumenti), il suono di avvio di default non sar&agrave; ancora ritornato.

In entrambi i casi, riavvia NVDA e tutto funzioner&agrave; correttamente.

Per gli sviluppatori, il motivo  &egrave; dato dal fatto che NVDA riproduce il suono di avvio prima dell'inizializazione degli addon.

## Cambiamenti nella 1.0 ##
* Versione iniziale.