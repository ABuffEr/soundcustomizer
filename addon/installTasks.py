﻿# -*- coding: UTF-8 -*-
import os
import sys
import shutil
import glob

addonRootPath=os.path.dirname(__file__)
exePath=sys.path[0].rsplit("\\", 1)[0]

def onUninstall():
	"""Restore default waves of NVDA"""
	for file in glob.glob(addonRootPath+"\\waves\\backup\\*.wav"):
		fileName=os.path.basename(file)
		shutil.move(file, exePath+"\\waves\\"+fileName)
