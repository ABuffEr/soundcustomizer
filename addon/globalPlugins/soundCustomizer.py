# -*- coding: UTF-8 -*-
#Author: Alberto Buffolino

import globalPluginHandler
import addonHandler
from configobj import *
import wave
import nvwave
import config
from logHandler import log
import os
import sys
import glob
import gui
import wx
import time
import speech
import globalVars
import shutil
from versionInfo import version

addonHandler.initTranslation()

# various useful path variables
addonRootPath=os.path.dirname(__file__).rsplit("\\", 1)[0]
myWavesPath=addonRootPath+"\\waves\\"
exePath=sys.path[0].rsplit("\\", 1)[0]
defaultWavesPath=exePath+"\\waves\\"
# initialize objects to store settings
iniFile=os.path.join(addonRootPath+"\\settings.ini")
myConf=ConfigObj(iniFile, encoding="UTF8")
# create and populate settings.ini file
if not os.path.isfile(iniFile):
	filesList=[file.rsplit("\\", 1)[1] for file in glob.glob(defaultWavesPath+"*.wav")]
	soundDict={k: "Default" for k in filesList}
	myConf["customSounds"]=soundDict
	myConf["lastVersion"]=version
	myConf.write()
# directory and subdirectory creation for waves
if not os.path.isdir(addonRootPath+"\\waves"):
	os.makedirs(addonRootPath+"\\waves")
	os.makedirs(addonRootPath+"\\waves\\temp")
	os.makedirs(addonRootPath+"\\waves\\backup")
	os.makedirs(addonRootPath+"\\waves\\associated")
	# save a backup of all original waves
	for file in glob.glob(defaultWavesPath+"*.wav"):
		shutil.copy(file, myWavesPath+"backup")
# manage post update
alertForRestart=False
if myConf["lastVersion"] != version:
	items=myConf["customSounds"].items()
	for (k,v) in items:
		if v != "Default":
			f1=os.stat(myWavesPath+"associated\\"+v)
			f2=os.stat(defaultWavesPath+k)
			if f1.st_size != f2.st_size:
				shutil.copy(myWavesPath+"associated\\"+v, defaultWavesPath+k)
				if k == "start.wav":
					alertForRestart=True
	myConf["lastVersion"]=version
	myConf.write()

def cleanAssociated():
	"""Clear Associated folder, keeping only useful files, based on configuration.
		Called by ok and cancel button associated event in GUI"""
	curAssociatedValues=myConf["customSounds"].values()
	for file in glob.glob(myWavesPath+"associated\\*.wav"):
		fileName=file.rsplit("\\", 1)[1]
		if not fileName in curAssociatedValues:
			os.remove(file)

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	def __init__(self, *args, **kwargs):
		super(GlobalPlugin, self).__init__(*args, **kwargs)
		if globalVars.appArgs.secure:
			return
		self.createMenu()

	def createMenu(self):
		self.prefsMenu=gui.mainFrame.sysTrayIcon.menu.GetMenuItems()[0].GetSubMenu()
		self.SoundCustomizerItem=self.prefsMenu.Append(wx.ID_ANY, _("Sound Customizer Settings..."), "")
		gui.mainFrame.sysTrayIcon.Bind(wx.EVT_MENU, lambda e: gui.mainFrame._popupSettingsDialog(SoundCustomizerSettingsDialog), self.SoundCustomizerItem)

	def terminate(self):
		try:
			self.prefsMenu.RemoveItem(self.SoundCustomizerItem)
		except wx.PyDeadObjectError:
			pass

class SoundCustomizerSettingsDialog(gui.SettingsDialog):

	# Translators: title of settings dialog
	title=_("Sound Customizer Settings")

	def __init__(self, parent):
		super(SoundCustomizerSettingsDialog, self).__init__(parent)
		# a backup of current configuration
		self.oldConf=dict(myConf["customSounds"].items())
		# list of original files which will be restored
		# if you cancel a absolute first customization
		self.movedOriginalFiles=[]
		# dict that keeps tracks of repeated customization in same session for each sound
		keys=myConf["customSounds"].keys()
		self.times={k: 0 for k in keys}
		# list of chosen files which will be restored
		# if you cancel some subsequent customizations
		self.movedTempFiles=[]

	def makeSettings(self, settingsSizer):
		# Translators: help text label
		helpLabel=wx.StaticText(self, label=_("Choose the sound that you want customize from the list, press browse to choose your file, and press space on the list element or use the dedicated buttons to listen the two sounds."))
		helpLabel.Wrap(settingsSizer.GetSize()[0])
		settingsSizer.Add(helpLabel)
		leftSizer=wx.BoxSizer(wx.VERTICAL)
		self.soundsList=wx.ListCtrl(self, style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
		# Translators: the first column header of sounds list
		self.soundsList.InsertColumn(0, _("Original"))
		# Translators: the second column header of sounds list
		self.soundsList.InsertColumn(1, _("Customized"))
		self.soundsList.Bind(wx.EVT_LIST_ITEM_FOCUSED, self.onListItemSelected)
		self.soundsList.Bind(wx.EVT_CHAR, self.onListChar)
		self.refreshList(0)
		leftSizer.Add(self.soundsList)
		rightSizer=wx.BoxSizer(wx.VERTICAL)
		# Translators: first play button label
		self.playOriginalSound=wx.Button(self, label=_("Play original sound"))
		self.playOriginalSound.Bind(wx.EVT_BUTTON, lambda evt, async=True: self.onPlayOriginalSound(evt, async))
		rightSizer.Add(self.playOriginalSound)
		# Translators: second play button label
		self.playCustomizedSound=wx.Button(self, label=_("Play customized sound"))
		self.playCustomizedSound.Bind(wx.EVT_BUTTON, lambda evt, async=True: self.onPlayCustomizedSound(evt, async))
		rightSizer.Add(self.playCustomizedSound)
		# Translators: browse button label
		self.browseForFile=wx.Button(self, label=_("Browse..."))
		self.browseForFile.Bind(wx.EVT_BUTTON, self.onBrowse)
		rightSizer.Add(self.browseForFile)
		# Translators: reset button label
		self.resetToDefault=wx.Button(self, label=_("Reset to default"))
		self.resetToDefault.Bind(wx.EVT_BUTTON, self.onReset)
		rightSizer.Add(self.resetToDefault)
		settingsSizer.Add(leftSizer)
		settingsSizer.Add(rightSizer)

	def postInit(self):
		global alertForRestart
		if alertForRestart:
			# Translators: update related message
			self.updateDialog(_("If you have heard the default start sound, don't worry: it has already been overwritten by your chosen sound, simply restart NVDA"))
			alertForRestart=False
		self.soundsList.SetFocus()

	def onListItemSelected(self, evt):
		self.index = evt.GetIndex()

	def refreshList(self, index):
		"""Populate sounds list from configuration keys and values, alfabetically"""
		self.soundsList.DeleteAllItems()
		items=myConf["customSounds"].items()
		items.sort()
		for (k, v) in items:
			self.soundsList.Append((k, v))
		self.soundsList.Select(index, on=1)
		self.soundsList.SetItemState(index, wx.LIST_STATE_FOCUSED, wx.LIST_STATE_FOCUSED)

	def onListChar(self, evt):
		"""Play original and customized sounds of current list item, pressing space"""
		if evt.KeyCode == wx.WXK_SPACE:
			speech.cancelSpeech()
			self.onPlayOriginalSound(wx.EVT_BUTTON, False)
			# insert a pause, to separate the two sounds
			time.sleep(0.27)
			self.onPlayCustomizedSound(wx.EVT_BUTTON)
		else:
			evt.Skip()

	def onPlayOriginalSound(self, evt, async=True):
		speech.cancelSpeech()
		oSound=self.soundsList.GetItem(self.index, 0).GetText()
		nvwave.playWaveFile(myWavesPath+"backup\\"+oSound, async)

	def onPlayCustomizedSound(self, evt, async=True):
		speech.cancelSpeech()
		cSound=self.soundsList.GetItem(self.index, 1).GetText()
		if cSound == "Default":
			self.onPlayOriginalSound(wx.EVT_BUTTON)
		else:
			oSound=self.soundsList.GetItem(self.index, 0).GetText()
			nvwave.playWaveFile(defaultWavesPath+oSound, async)

	def onBrowse(self, evt):
		# Translators: message for file open dialog
		self.fileDialog=wx.FileDialog(self, message=_("Select your preferred .wav file"), wildcard="*.wav", style=wx.FD_OPEN)
		if self.fileDialog.ShowModal() != wx.ID_OK:
			self.soundsList.SetFocus()
			return
		# complete path of chosen file
		src=self.fileDialog.GetPath()
		# verify if chosen file is compatible
		try:
			wave.open(src, "r")
		except:
			# Translators: message for invalid format alert dialog
			self.alertDialog(_("File format not supported, please choose another .wav file"))
			return
		oSound=self.soundsList.GetItem(self.index, 0).GetText()
		global myConf
		# old file name associated with NVDA sound
		oldFile=myConf["customSounds"][oSound]
		# new chosen file name
		newFile=self.fileDialog.GetFilename()
		if oldFile == "Default":
			# add original sound name to movedOriginalFiles,
			# so it will be restored in defaultWavesPath if you cancel changes
			self.movedOriginalFiles.append(oSound)
		else:
			# current index, the number of previous choices
			# (starting from a non-default associated sound)
			index=self.times[oSound]
			# move the old chosen file to temp folder, renaming with index prefix
			shutil.move(defaultWavesPath+oSound, myWavesPath+"temp\\"+str(index)+oSound)
			# add the file name in temp to movedTempFiles list,
			# so it will be restored in defaultWavesPath (only if index 0, the oldest), if you cancel changes,
			# or deleted (all index) if you confirm
			self.movedTempFiles.append(str(index)+oSound)
			self.times[oSound]=index+1
		# copy chosen file to defaultWavesPath, renaming it correctly
		shutil.copy(src, defaultWavesPath+oSound)
		# copy also to Associated folder, without rename
		shutil.copy(src, myWavesPath+"associated\\"+newFile)
		# update configuration (without saving, for now)
		if src == defaultWavesPath+oSound:
			myConf["customSounds"][oSound]="Default"
		else:
			myConf["customSounds"][oSound]=newFile
		self.refreshList(self.index)
		self.soundsList.SetFocus()

	def onReset(self, evt):
		oSound=self.soundsList.GetItem(self.index, 0).GetText()
		global myConf
		oldFile=myConf["customSounds"][oSound]
		# manage case of reset after some choices, in same session,
		# and the operations for an eventual undo;
		# finally, update configuration
		if oldFile != "Default":
			index=self.times[oSound]
			shutil.move(defaultWavesPath+oSound, myWavesPath+"temp\\"+str(index)+oSound)
			self.movedTempFiles.append(str(index)+oSound)
			self.times[oSound]=index+1
			shutil.copy(myWavesPath+"backup\\"+oSound, defaultWavesPath+oSound)
			myConf["customSounds"][oSound]="Default"
			self.refreshList(self.index)
		self.soundsList.SetFocus()

	def onOk(self, evt):
		super(SoundCustomizerSettingsDialog, self).onOk(evt)
		# Finally, write Configuration
		global myConf
		myConf.write()
		# remove all files in temp folder
		for file in self.movedTempFiles:
			os.remove(myWavesPath+"temp\\"+file)
		cleanAssociated()

	def onCancel(self, evt):
		# undo changes
		global myConf
		myConf["customSounds"]=self.oldConf
		# restore file whose name starts with 0 in defaultWavesPath
		# delete all others
		for file in self.movedTempFiles:
			if file[0]=="0":
				shutil.move(myWavesPath+"temp\\"+file, defaultWavesPath+file[1:])
			else:
				os.remove(myWavesPath+"temp\\"+file)
		# restore default files, if this is the initial configuration
		for file in self.movedOriginalFiles:
			shutil.copy(myWavesPath+"backup\\"+file, defaultWavesPath+file)
		cleanAssociated()
		super(SoundCustomizerSettingsDialog, self).onCancel(evt)

	def alertDialog(self, message):
		# Translators: the messageBox type for alertDialog
		gui.messageBox(message, _("Error"), wx.OK | wx.ICON_ERROR)
		self.onBrowse(wx.EVT_BUTTON)

	def updateDialog(self, message):
		# Translators: the messageBox type for updateDialog
		gui.messageBox(message, _("Warning"), wx.OK | wx.ICON_WARNING)
