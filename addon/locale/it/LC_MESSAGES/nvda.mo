��          �      ,      �  '   �  	   �  �   �  
   p     {  :   �  �   �     >     G     ]     q     �     �     �     �     �  D  �  D   7  
   |  �   �     D     S  A   Z  �   �  	   )      3     T     p  $   �     �      �  #   �  
   �            	   
                                                               An addon to customize NVDA .wav sounds. Browse... Choose the sound that you want customize from the list, press browse to choose your file, and press space on the list element or use the dedicated buttons to listen the two sounds. Customized Error File format not supported, please choose another .wav file If you have heard the default start sound, don't worry: it has already been overwritten by your chosen sound, simply restart NVDA Original Play customized sound Play original sound Reset to default Select your preferred .wav file Sound Customizer Sound Customizer Settings Sound Customizer Settings... Warning Project-Id-Version: Sound Customizer 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-03 21:39+0100
PO-Revision-Date: 2013-09-03 21:43+0100
Last-Translator: Alberto Buffolino <a.buffolino@gmail.com>
Language-Team: Alberto Buffolino <a.buffolino@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: ..\..\..\globalPlugins
X-Poedit-SearchPath-1: ..\..\..\..
 Un componente aggiuntivo per personalizzare i suoni in .wav di NVDA. Sfoglia... Scegli il suono che vuoi personalizzare dalla lista, premi sfoglia per scegliere il tuo file, e premi spazio su un elemento della lista o usa i pulsanti dedicati per ascoltare i due suoni. Personalizzato Errore Formato file non supportato, per favore scegli un altro file .wav Se hai sentito il suono di avvio di default, non preoccuparti: è già stato sovrascritto dal suono scelto da te, riavvia semplicemente NVDA Originale Riproduci il file personalizzato Riproduci il file originale Reimposta a default Seleziona il tuo file .wav preferito Sound Customizer Impostazioni di Sound Customizer Impostazioni di Sound Customizer... Attenzione 